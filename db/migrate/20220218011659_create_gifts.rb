class CreateGifts < ActiveRecord::Migration[7.0]
  def change
    create_table :gifts do |t|
      t.string :name
      t.integer :price
      t.string :image
      t.text :description
      t.text :notes

      t.timestamps
    end

    # HABTM table 
    create_table :gifts_orders, id: false do |t|
      t.belongs_to :gift
      t.belongs_to :order
    end
  end
end
