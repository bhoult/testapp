require "test_helper"

class OrderTest < ActiveSupport::TestCase
  
  def setup
    @school = School.create(name:"testschool")
    @address = Address.create_random
    @school.address = @address

    @recipient = Recipient.create_random
    @recipient_address = Address.create_random
    @recipient.address = @recipient_address
    @recipient.school = @school
    @recipient.save

    @order = Order.create_random
    @order.school = @school
    @order.recipients << @recipient
    @order.gifts << Gift.first
    @order.save

    @school.save!
  end

  test "setup order is valid" do
    assert @order.valid? == true, "Order in setup is not valid"
  end

  test "order must have a school" do
    @order.school = nil
    refute @order.valid?, "Order was valid without a school"
    assert_not_nil @order.errors[:school], 'no validation error for order without school'
  end

  test "order must have a recipient" do
    assert @order.recipients.count == 1, "Order should have one recipient."
    
    @order.recipients.delete_all
    refute @order.valid?, "Order was valid without a recipient"
    assert_not_nil @order.errors[:recipients], 'no validation error for order without recipients'
  end

  test "order must have a gift" do
    assert @order.gifts.count == 1, "Order should have one gift."
    
    @order.gifts.delete_all
    refute @order.valid?, "Order was valid without a gift"
    assert_not_nil @order.errors[:gifts], 'no validation error for order without gifts'
  end

  test "order cannot have more than 20 recipients per order" do    
    # Add 20 to the original 1
    for i in 1..20
      r = Recipient.create_random
      r.address = Address.create_random
      r.school = @school
      @order.recipients << r
    end
    assert @order.recipients.count == 20, "more than 20 recipients were added to an order"
    assert_not_nil @order.errors, 'no validation error for order with too many recipients'
  end

  test "school cannot order more than 60 gifts total per day" do
    # School has one gift ordered.  Make 2 more orders with two gifts and 10 recipients each for 41 total gifts
    for oi in 1..2
      order = Order.create_random
      order.school = @school
      order.gifts << Gift.last
      order.gifts << Gift.first
      for ri1 in 1..10
        order.recipients << Recipient.all.sample
      end
      order.save!
    end
    assert @order.valid?, "Order was invalid with 41 total school gifts on the same day gifts"

    # Now try making another order with 10 recipients and two gifts fore a total of 61 gifts  
    for oi in 1..1
      order = Order.create_random
      order.school = @school
      order.gifts << Gift.last
      order.gifts << Gift.first
      for ri1 in 1..10
        order.recipients << Recipient.all.sample
      end
      order.save!
    end
    refute order.valid?, "Order was invalid with more than 60 total school gifts on the same day gifts"
    assert_not_nil @order.errors, 'no validation error for order after 60 gifts in a school in a day'
  end

  test "cannot add duplicate recipients to an order" do
    x = @order.recipients.count
    @order.recipients <<  @order.recipients.first
    assert @order.recipients.count == x, "Duplicate recipient was added to the order"
    assert_not_nil @order.errors, "Error was not generated when duplicate recipient was added to order"
  end 

end
