#resources :orders
#   # All order operations are scoped to a school
#   get "/schools/:id/orders" => "orders#index"                                           # index
#   get "/schools/:id/orders/:oid" => "orders#read"                                       # read
#   post "/schools/:id/orders" => "orders#create"                                         # create
#   patch "/schools/:id/orders/:oid" => "orders#update"                                   # update
#   delete "/schools/:id/orders/:oid" => "orders#delete"                                  # delete
#   patch "/schools/:id/orders/:oid/cancel" => "orders#cancel"                            # cancel order
#   patch "/schools/:id/orders/:oid/ship" => "orders#ship"                                # ship order
#   # Thought it would be simpler to allow adding and removing individual gifts and recipients from orders

require "test_helper"

class OrdersControllerTest < ActionDispatch::IntegrationTest

  def setup
    # Everything is gonna need a token!
    @user = User.create(username:'admin', firstname:'Admin', lastname:'User', email:'adminuser@email.com', password:'test', notes:"Default Admin User", admin:true)
    post "/users/login", params: {username:"admin", password:"test"}, as: :multipart_form
    @token = JSON.parse(body)["token"] 

    @recipient = Recipient.create_random
    @recipient2 = Recipient.create_random
    @address = Address.create_random
    @school = School.create_random
    
    @school.address = @address
    @school.save!
    @recipient.address = @address
    @recipient.school = @school
    @recipient.save!
    @recipient2.address = @address
    @recipient2.school = @school
    @recipient2.save!

    @order = Order.create_random
    @order.status_id = 1 # can't be shipped
    @order.school = @school
    @order.gifts << Gift.first
    @order.recipients << @recipient
    @order.save!
  end

  test "should not be able to hit anything without token" do
    get "/schools/#{@school.id}/orders"
    assert_response :unauthorized
    assert_match  /log in/, body

    get "/schools/#{@school.id}/orders/#{@order.id}"
    assert_response :unauthorized
    assert_match  /log in/, body

    post "/schools/#{@school.id}/orders"
    assert_response :unauthorized
    assert_match  /log in/, body

    patch "/schools/#{@school.id}/orders/#{@order.id}"
    assert_response :unauthorized
    assert_match  /log in/, body

    delete "/schools/#{@school.id}/orders/#{@order.id}"
    assert_response :unauthorized
    assert_match  /log in/, body
  end

  test "get orders index" do
    get "/schools/#{@school.id}/orders", headers: {Authorization: "Bearer #{@token}"}
    assert_response :success
    assert_match  /Randomly/, body
  end

  test "get a orders info" do
    get "/schools/#{@school.id}/orders/#{@order.id}", headers: {Authorization: "Bearer #{@token}"}
    assert_response :success
    assert_match  /Randomly/, body
  end

  test "create an order" do
    post "/schools/#{@school.id}/orders", headers: {Authorization: "Bearer #{@token}"}, 
        params: {notes:"order_test", email_recipients:"true", status:"1", gift_id:"1", recipient_id:@recipient2.id}, as: :multipart_form
    assert_response :success
    assert Order.count == 2
    assert_match  /order_test/, body
  end

  test "update an order" do
    patch "/schools/#{@school.id}/orders/#{@order.id}", headers: {Authorization: "Bearer #{@token}"}, 
        params: {notes:"order_test", email_recipients:"true", status:"1", gift_id:"1", recipient_id:@recipient.id}, as: :multipart_form
    assert_response :success
    assert Order.count == 1
    assert_match  /order_test/, body
  end

  test "delete an order" do
    delete "/schools/#{@school.id}/orders/#{@order.id}", headers: {Authorization: "Bearer #{@token}"}
    assert_response :success
    assert Order.count == 0
    assert_match  /Randomly Generated/, body
  end

  #   post "/schools/:id/orders/:oid/add_recipient/:rid" => "orders#add_recipient"          # add recipient
  #   delete "/schools/:id/orders/:oid/remove_recipient/:rid" => "orders#remove_recipient"  # remove recipient
  #   post "/schools/:id/orders/:oid/add_gift/:gid" => "orders#add_gift"                    # add gift
  #   delete "/schools/:id/orders/:oid/remove_gift/:gid" => "orders#remove_gift"            # remove gift

  test "add and remove a recipient" do
    assert @order.recipients.count == 1
    # Add a recipient
    post "/schools/#{@school.id}/orders/#{@order.id}/add_recipient/#{@recipient2.id}", headers: {Authorization: "Bearer #{@token}"}
    assert_response :success
    assert @order.recipients.count == 2
    assert_match  /Randomly Generated/, body
    # Remove a recipient
    delete "/schools/#{@school.id}/orders/#{@order.id}/remove_recipient/#{@recipient2.id}", headers: {Authorization: "Bearer #{@token}"}
    assert_response :success
    assert @order.recipients.count == 1
    assert_match  /Randomly Generated/, body
  end

  test "add and remove a gift" do
    assert @order.gifts.count == 1
    # Add a recipient
    post "/schools/#{@school.id}/orders/#{@order.id}/add_gift/2", headers: {Authorization: "Bearer #{@token}"}
    assert_response :success
    assert @order.gifts.count == 2
    assert_match  /Randomly Generated/, body
    # Remove a recipient
    delete "/schools/#{@school.id}/orders/#{@order.id}/remove_gift/2", headers: {Authorization: "Bearer #{@token}"}
    assert_response :success
    assert @order.gifts.count == 1
    assert_match  /Randomly Generated/, body
  end

end

