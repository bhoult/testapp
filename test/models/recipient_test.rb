require "test_helper"

class RecipientTest < ActiveSupport::TestCase
  
  def setup
    @school = School.create(name:"testschool")
    @address = Address.create_random
    @school.address = @address

    @recipient = Recipient.create_random
    @recipient_address = Address.create_random
    @recipient.address = @recipient_address
    @recipient.school = @school
    @recipient.save

    @order = Order.create_random
    @order.school = @school
    @order.recipients << @recipient
    @order.gifts << Gift.first
    @order.save

    @school.save!
  end

  test "setup recipient is valid" do
    assert @recipient.valid? == true, "Recipient in setup is not valid"
  end

  test "recipient must have a name" do
    @recipient.name = nil
    refute @recipient.valid?, "Recipient was saved without a name"
    assert_not_nil @recipient.errors[:name], 'no validation error for recipient name'
  end

  test "recipient must have an address" do
    @recipient.address = nil
    refute @recipient.valid?, "Recipient was saved without an address"
    assert_not_nil @recipient.errors[:address], 'no validation error for recipient without address'
  end

  test "recipient must have a school" do
    @recipient.school = nil
    refute @recipient.valid?, "Recipient was saved without a school"
    assert_not_nil @recipient.errors[:school], 'no validation error for recipient without school'
  end

  test "recipient has an order" do
    assert @recipient.orders.count == 1, "Recipient should have one order"
  end

end
