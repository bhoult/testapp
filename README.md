<h2>Welcome</h2>
This is a test app created for Aptegy to demonstrate api creation in Ruby on Rails.  It provides an api to facilitatate an application in which gifts and recipients can
be added to orders for different schools.
<h2>Requirements</h2>
The application was developed Feb 21, 2022 and tested using 
<a href="https://www.ruby-lang.org/en/" target="_blank">Ruby 3.1.1</a>, 
<a href="https://rubyonrails.org/" target="_blank">Rails 7.0.22</a> and
<a href="https://www.postgresql.org/" target="_blank">Postgres 13.5</a> on 
<a href="https://ubuntu.com/" target="_blank">Ubuntu Linux 21.10</a> 
<h2>Installation</h2>
From Ubuntu Linux:

1. Install Postgresql and git
```
$ sudo apt-get install postgresql git libpq-dev git
```

2. Login as postgres user and create the default role (or modify in /config/database.yml) 
assuming your user is authorized. Also give your user permission to create databases (modify <USERNAME> below).
```
$ sudo -u postgres psql
# create user <USERNAME> with password <PASSWORD>;
# create database <USERNAME>;
# create role testapp with createdb login password 'asdkjfhr932485r7'; 
# ALTER USER <USERNAME> CREATEDB;
# \q
```

3. Install <a href="https://rvm.io/rvm/install" target="_blank">RVM</a>, update, Then install the latest 
ruby (3.1.1 at the time of this writing)
```
$ rvm get head
$ rvm install ruby-3.1.1
```
Instructions for rvm are included in the above link, but there are many alternitive ways to install ruby... 
just make sure it is at least version 3.1.1

4. Clone the source code for this project and cd into it when done.
```
$ git clone https://bhoult@bitbucket.org/bhoult/testapp.git</br>
$ cd testapp
```  
 
5. Install rails and the bundle of gems in the Gemfile
```
$ gem install rails</br>
$ rm Gemfile.lock
$ bundle install
```  

6. Setup the database and generate some dummy data.  (if seed fails run again)
```
$ rails db:migrate:reset; rails db:seed</br>
```  

7. Run the tests to make sure everything works.
```
$ rails test
```

8. Start the server
```
$ rails s -b 0.0.0.0</br>
```
Then point your web browser to localhost:3000 


<p>
To interact with the api you will need to generate a <a href="https://jwt.io/" target="_blank">JSON Web Token</a>.  This is a bearer token that needs to be included in the header of any api requests.  
To simplify it a bit you can also include it in GET requests from the browser the token in the below example should work for the default user.
<code>http://localhost:3000/statuses?bearer=eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8</code>
</p>

<p>
To login as a user and retrieve the token you can do the following from the command line <code>$ curl -X POST localhost:3000/users/login -H 'Content-Type: application/json' -d '{"username":"admin","password":"test"}'| json_pp</code>
</p>

<p>
If you wish to containerize this application there is a Dockerfile included in the root folder to get you started.
</p>

<p>
There are example CURL calls for each api located in the source code under app/controllers.  The varous api calls are further documented in the root index of the app when it is running.
</p>