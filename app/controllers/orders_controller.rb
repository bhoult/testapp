class OrdersController < ApplicationController
    before_action :authorized

    # GET /schools/:id/orders
    # curl -X GET -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1/orders | json_pp
    def index
        slog("All School Orders")
        render json: Order.where(school_id: params[:id]).to_json(:include => [:recipients, :gifts] )
    end

    # GET /schools/:id/orders/:oid
    # curl -X GET -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1/orders/1 | json_pp
    def read
        slog("Individual School Order")
        render json: Order.where(school_id: params[:id]).where(id: params[:oid]).first.to_json(:include => [:recipients, :gifts] )
    end
  
    # POST /schools/:id/orders
    # A gift type and at least one Recipient are required.
    # curl -X POST -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1/orders -H 'Content-Type: application/json' -d '{"notes":"order_test", "email_recipients":"true", "status":"1", "gift_id":"1", "recipient_id":"1"}' | json_pp
    def create
        order = Order.new(
            notes: params[:notes],
            email_recipients: params[:email_recipients],
            school: School.find_by(id: params[:id]),
            status: Status.find_by(id: params[:status])
        )
        gift = Gift.find_by(id: params[:gift_id])
        order.gifts << gift unless gift.nil?
        recipient = Recipient.find_by(id: params[:recipient_id])
        order.recipients << recipient unless recipient.nil?

        if order.save
            slog("Create Order")
            render json: order.to_json(:include => [:recipients, :gifts] )
        else 
            elog("Create Order")
            render json: order.errors, status: 500
        end
    end

    # PATCH /schools/:id/orders/:oid
    # curl -X PATCH -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1/orders/1 -H 'Content-Type: application/json' -d '{"notes":"order_update_test", "email_recipients":"false", "status":"3"}' | json_pp
    def update
        order = Order.where(id: params[:oid]).where(school_id: params[:id]).first

        shipped = Status.find_by(name: "ORDER_SHIPPED")
        return render json: {error: "Order has already shipped.  Cannot update."}, status: 500 if order.status == shipped
        
        unless order.nil?
            order.update(
                notes: params[:notes],
                email_recipients: params[:email_recipients],
                school: School.find_by(id: params[:id]),
                status: Status.find_by(id: params[:status])
            )
            
            if order.valid?
                slog("Update Order")
                render json: order.to_json(:include => [:recipients, :gifts] )
            else 
                slog("Update Order")
                render json: order.errors, status: 500
            end
        else
            elog("Update Order")
            render json: {error: "Could not find order for school #{params[:id]} with id #{params[:oid]} to update"}, status:500
        end
    end

    # DELETE /schools/:id/orders/:rid
    # curl -X DELETE -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1/orders/1 | json_pp
    def delete
        order = Order.where(id: params[:oid]).where(school_id: params[:id])
        unless order.empty?
            order.first.destroy!
            slog("Delete Order")
            render json: order.first.to_json(:include => [:recipients, :gifts] )
        else
            elog("Delete Order")
            render json: {error: "Could not find recipient with id #{params[:rid]} to delete"}, status: 500
        end
    end

    # POST /schools/:id/orders/:oid/add_recipient/:rid
    # curl -X POST -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1/orders/1/add_recipient/2 | json_pp
    def add_recipient
        order = Order.where(school_id: params[:id]).where(id: params[:oid]).first
        recipient = Recipient.where(school_id: params[:id]).where(id: params[:rid]).first
        
        shipped = Status.find_by(name: "ORDER_SHIPPED")
        if order.status == shipped
            elog("Add Recipient") 
            return render json: {error: "Order has already shipped.  Cannot update."}, status: 500 
        end
        if order.nil?
            elog("Add Recipient") 
            return render json: {error: "Order not found"}, status: 500 
        end
        if recipient.nil?
            elog("Add Recipient") 
            return render json: {error: "Recipient not found"}, status: 500 
        end
        if order.recipients.member?(recipient)
            elog("Add Recipient") 
            return render json: {error: "Recipient already in order"}, status: 500 
        end
        
        order.recipients << recipient
        slog("Add Recipient")
        render json: order.to_json(:include => [:recipients, :gifts] )
    end 

    # DELETE /schools/:id/orders/:oid/remove_recipient/:rid
    # curl -X DELETE -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1/orders/1/remove_recipient/2 | json_pp
    def remove_recipient
        order = Order.where(school_id: params[:id]).where(id: params[:oid]).first
        recipient = Recipient.where(school_id: params[:id]).where(id: params[:rid]).first

        shipped = Status.find_by(name: "ORDER_SHIPPED")
        if order.status == shipped
            elog("Remove Recipient") 
            return render json: {error: "Order has already shipped.  Cannot update."}, status: 500 
        end
        if order.nil?
            elog("Remove Recipient") 
            return render json: {error: "Order not found"}, status: 500 
        end
        if recipient.nil?
            elog("Remove Recipient") 
            return render json: {error: "Recipient not found"}, status: 500 
        end
        unless order.recipients.member?(recipient)
            elog("Remove Recipient") 
            return render json: {error: "Recipient already in order"}, status: 500 
        end
        unless order.recipients.length > 1
            elog("Remove Recipient") 
            return render json: {error: "Order must have at least one recipient"}, status: 500 
        end
        
        order.recipients.delete(recipient)
        slog("Remove Recipient") 
        render json: order.to_json(:include => [:recipients, :gifts] )
    end 

    # POST /schools/:id/orders/:oid/add_gift/:gid" => "orders#add_gift
    # curl -X POST -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1/orders/1/add_gift/2 | json_pp
    def add_gift
        order = Order.where(school_id: params[:id]).where(id: params[:oid]).first
        gift = Gift.where(id: params[:gid]).first

        shipped = Status.find_by(name: "ORDER_SHIPPED")
        if order.status == shipped
            elog("Add Gift")
            return render json: {error: "Order has already shipped.  Cannot update."}, status: 500 
        end
        if order.nil?
            elog("Add Gift")
            return render json: {error: "Order not found"}, status: 500 
        end
        if gift.nil?
            elog("Add Gift")
            return render json: {error: "Gift not found"}, status: 500 
        end
        if order.gifts.member?(gift)
            elog("Add Gift")
            return render json: {error: "Gift already in order"}, status: 500 
        end
        
        order.gifts << gift
        render json: order.to_json(:include => [:recipients, :gifts] )
    end 
    
    # DELETE /schools/:id/orders/:oid/remove_gift/:gid
    # curl -X DELETE -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1/orders/1/remove_gift/2 | json_pp
    def remove_gift
        order = Order.where(school_id: params[:id]).where(id: params[:oid]).first
        gift = Gift.where(id: params[:gid]).first

        shipped = Status.find_by(name: "ORDER_SHIPPED")
        if order.status == shipped
            elog("Remove Gift")
            return render json: {error: "Order has already shipped.  Cannot update."}, status: 500 
        end
        if order.nil?
            elog("Remove Gift")
            return render json: {error: "Order not found"}, status: 500 
        end
        if gift.nil?
            elog("Remove Gift")
            return render json: {error: "Gift not found"}, status: 500 
        end
        unless order.gifts.member?(gift)
            elog("Remove Gift")
            return render json: {error: "Gift already in order"}, status: 500 
        end
        unless order.gifts.length > 1
            elog("Remove Gift")
            return render json: {error: "Order must have at least one gift"}, status: 500 
        end
        
        order.gifts.delete(gift)
        slog("Delete Gift")
        render json: order.to_json(:include => [:recipients, :gifts] )
    end 

    # PATCH /schools/:id/orders/:oid/cancel
    # curl -X PATCH -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1/orders/1/cancel | json_pp
    def cancel
        cancelled = Status.find_by(name: "ORDER_CANCELLED")
        order = Order.where(school_id: params[:id]).where(id: params[:oid]).first
        
        # Error if order is not found
        if order.nil?
            elog("Cancel Order")
            return render json: {error: "Order not found"}, status: 500 
        end
        
        # Update
        order.status = cancelled
        order.save!
        slog("Cancel Order")
        render json: order.to_json(:include => [:recipients, :gifts] )
    end

    # PATCH /schools/:id/orders/:oid/ship
    # curl -X PATCH -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1/orders/1/ship | json_pp
    def ship
        shipped = Status.find_by(name: "ORDER_SHIPPED")
        order = Order.where(school_id: params[:id]).where(id: params[:oid]).first
        
        # Error if order is not found
        if order.nil?
            elog("Ship Order")
            return render json: {error: "Order not found"}, status: 500 
        end
        
        # Send Email
        order.send_shipped_notification

        # Update
        order.status = shipped
        order.save!
        slog("Ship Order")
        render json: order.to_json(:include => [:recipients, :gifts] )
    end     
end