#resources :schools
#get "/schools" => "schools#index"         # index
#get "/schools/:id" => "schools#read"      # read
#post "/schools" => "schools#create"       # create
#patch "/schools/:id" => "schools#update"  # update
#delete "/schools/:id" => "schools#delete" # delete

require "test_helper"

class SchoolsControllerTest < ActionDispatch::IntegrationTest

  def setup
    # Everything is gonna need a token!
    @user = User.create(username:'admin', firstname:'Admin', lastname:'User', email:'adminuser@email.com', password:'test', notes:"Default Admin User", admin:true)
    post "/users/login", params: {username:"admin", password:"test"}, as: :multipart_form
    @token = JSON.parse(body)["token"] 

    @school = School.create_random
    @address = Address.create_random
    @school.address = @address
    @school.save!
  end

  test "should not be able to hit anything without token" do
    get "/schools"
    assert_response :unauthorized
    assert_match  /log in/, body

    get "/schools/1"
    assert_response :unauthorized
    assert_match  /log in/, body

    post "/schools"
    assert_response :unauthorized
    assert_match  /log in/, body

    patch "/schools/1"
    assert_response :unauthorized
    assert_match  /log in/, body

    delete "/schools/1"
    assert_response :unauthorized
    assert_match  /log in/, body
  end

  test "get schools index" do
    get "/schools", headers: {Authorization: "Bearer #{@token}"}
    assert_response :success
    assert_match  /Randomly/, body
  end

  test "get a school info" do
    get "/schools/#{@school.id}", headers: {Authorization: "Bearer #{@token}"}
    assert_response :success
    assert_match  /Randomly/, body
  end

  test "create a school" do
    post "/schools", headers: {Authorization: "Bearer #{@token}"}, 
        params: {name:"school_test",street1:"somestreet",city:"somecity",state:"arkansas",zip:"12345"}, as: :multipart_form
    assert_response :success
    assert School.count == 2
    assert_match  /school_test/, body
  end

  test "update a school" do
    patch "/schools/#{@school.id}", headers: {Authorization: "Bearer #{@token}"}, 
        params: {name:"school_test",street1:"somestreet",city:"somecity",state:"arkansas",zip:"12345"}, as: :multipart_form
    assert_response :success
    assert School.count == 1
    assert_match  /school_test/, body
  end

  test "delete a school" do
    delete "/schools/#{@school.id}", headers: {Authorization: "Bearer #{@token}"}
    assert_response :success
    assert School.count == 0
    assert_match  /#{@school.name}/, body
  end

end
