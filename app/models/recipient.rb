class Recipient < ApplicationRecord
    belongs_to :school # If recipient has multiple children in different schools then they will get multiple Recipient records.
    has_and_belongs_to_many :orders  # A recipient can be in multiple orders
    has_one :address, :as => :addressable, dependent: :destroy

    validates :name, presence: true
    validates :address, presence: true
    validates :school, presence: true 

    # Make a ramdom recipient... but cant save without address and school
    def self.create_random
        Recipient.new(
            name: Faker::Name.name,
            email: Faker::Internet.email,
            notes: 'Randomly Generated Fake Record'
        )
    end
end
