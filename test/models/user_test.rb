require "test_helper"

class UserTest < ActiveSupport::TestCase

  def setup
    @user = User.create(username:'admin', firstname:'Admin', lastname:'User', email:'adminuser@email.com', password:'test', notes:"Default Admin User", admin:true)
  end
  
  test "user count working" do
    User.create(username:'admin2', firstname:'Admin', lastname:'User', email:'adminuser@email.com', password:'test', notes:"Default Admin User", admin:true)
    assert_equal 2, User.count
  end

  test "user password working" do 
    assert_equal @user, @user.authenticate('test')
  end

  test "user must have username" do
    @user.username = nil
    refute @user.valid?, "User was saved without a name"
    assert_not_nil @user.errors[:username], 'no validation error for username'
  end

  test "user must have password" do
    @user.password = nil
    refute @user.valid?, "User was saved without a password"
    assert_not_nil @user.errors[:password], 'no validation error for password'
  end

  test "user name must be unique" do
    u = User.create(username:'admin', firstname:'Admin', lastname:'User', email:'adminuser@email.com', password:'test', notes:"Default Admin User", admin:true)
    refute u.valid?, "Duplicate user was created"
    assert_not_nil @user.errors, 'no validation error for duplicate user'
  end

end
