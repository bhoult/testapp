class CreateAddresses < ActiveRecord::Migration[7.0]
  def change
    create_table :addresses do |t|
      t.string :street1
      t.string :street2
      t.string :city
      t.string :state
      t.string :zip
      t.string :country
      t.text :notes
      t.bigint :addressable_id
      t.string :addressable_type

      t.timestamps
    end
  end
end
