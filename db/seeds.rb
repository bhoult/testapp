# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

# Add the default User
puts "Add the default admin user"
#crypt = ActiveSupport::MessageEncryptor.new(Rails.configuration.secret)
#encrypted_password = crypt.encrypt_and_sign("test")  #password = crypt.decrypt_and_verify(encrypted_password)
User.new(username:'admin', firstname:'Admin', lastname:'User', email:'adminuser@email.com', password:'test', notes:"Default Admin User", admin:true).save!

# Add the default gifts.
puts "Add the gifts"
Gift.create(name: 'MUG', price: 1055, description: 'A fancy mug for your coffee!')
Gift.create(name: 'T_SHIRT', price: 1530, description: 'No shirt no service')
Gift.create(name: 'HOODIE', price: 2500, description: 'Because your LEET')
Gift.create(name: 'STICKER', price: 310, description: 'Stuck on someting')

# Add the default statuses.
puts "Add the statuses"
Status.create(name: 'ORDER_RECEIVED')
Status.create(name: 'ORDER_PROCESSING')
Status.create(name: 'ORDER_SHIPPED')
Status.create(name: 'ORDER_CANCELLED')

# Make a default schools
puts "Add the default schools"
s1 = School.create_random
s2 = School.create_random

# Make some default recipients
puts "Add some random recipients"
for i in 1..10 do
    # For school 1 
    r = Recipient.create_random
    r.school =  s1 # Add school
    r.address = Address.create_random
    r.save!
    # For school 2
    r = Recipient.create_random
    r.school =  s2 # Add school
    r.address = Address.create_random
    r.save!
end

# Make some default orders
print "Add some random orders"
for i in 1..10 do 
    print "."
    o1 = Order.create_random
    o1.school = s1
    o2 = Order.create_random
    o2.school = s2    

    # Add a random number of recipients to the order
    for r in 0..rand(2) do
        o1.recipients << s1.recipients.sample
        o2.recipients << s2.recipients.sample
    end

    # Add a random numbner of gifts to the order
    for g in 0..rand(2) do
        o1.gifts << Gift.all.sample
        o2.gifts << Gift.all.sample
    end

    o1.save!
    o2.save!
end
puts "."
