# Made this a model/controller assuming that the statuses might change in the future

class StatusesController < ApplicationController
    before_action :authorized

    # GET /statuses
    # curl -X GET -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/statuses | json_pp
    def index
        slog("statuses index")
        render json: Status.all
    end
end