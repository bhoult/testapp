# This will dockerize the rails app assuming that the postgres database is 
# already setup and the user is configured in docker containers host.  
# See readme for instuctions

# from inside rails application root folder: 
# 1. docker build -t aptegytest .
# 2. docker run -it --network=host -p 3000:3000 aptegytest

# -- for traefik
# docker commit 5b9e5cadd174
# docker save f8e33cbcfb00 | gzip -c > aptegytest.tar.gz
# scp aptegytest.tar.gz docker.hoult.co:.
# -- on destination
# docker load -i aptegytest.tar.gz
# docker run -d --network=host -l traefik.port=3000 -l traefik.frontend.rule=Host:aptegytest.hoult.co --restart always --name aptegytest aptegytest
# -- you have to configure the host postgres to allow external connections, and change the database config in rails to point to the host.

FROM ruby:3.1.1

RUN apt-get update && apt-get install -y npm && npm install -g yarn

RUN mkdir -p /var/app
COPY . /var/app
WORKDIR /var/app

RUN rm Gemfile.lock
RUN bundle install

CMD rails s -b 0.0.0.0