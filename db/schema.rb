# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_02_18_013613) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "addresses", force: :cascade do |t|
    t.string "street1"
    t.string "street2"
    t.string "city"
    t.string "state"
    t.string "zip"
    t.string "country"
    t.text "notes"
    t.bigint "addressable_id"
    t.string "addressable_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gifts", force: :cascade do |t|
    t.string "name"
    t.integer "price"
    t.string "image"
    t.text "description"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "gifts_orders", id: false, force: :cascade do |t|
    t.bigint "gift_id"
    t.bigint "order_id"
    t.index ["gift_id"], name: "index_gifts_orders_on_gift_id"
    t.index ["order_id"], name: "index_gifts_orders_on_order_id"
  end

  create_table "orders", force: :cascade do |t|
    t.text "notes"
    t.bigint "status_id"
    t.bigint "school_id"
    t.boolean "email_recipients"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders_recipients", id: false, force: :cascade do |t|
    t.bigint "order_id"
    t.bigint "recipient_id"
    t.index ["order_id"], name: "index_orders_recipients_on_order_id"
    t.index ["recipient_id"], name: "index_orders_recipients_on_recipient_id"
  end

  create_table "recipients", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.text "notes"
    t.bigint "school_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "recipients_schools", id: false, force: :cascade do |t|
    t.bigint "recipient_id"
    t.bigint "school_id"
    t.index ["recipient_id"], name: "index_recipients_schools_on_recipient_id"
    t.index ["school_id"], name: "index_recipients_schools_on_school_id"
  end

  create_table "schools", force: :cascade do |t|
    t.string "name"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "statuses", force: :cascade do |t|
    t.string "name"
    t.text "notes"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "firstname"
    t.string "lastname"
    t.string "username"
    t.string "password_digest"
    t.string "email"
    t.text "notes"
    t.boolean "admin"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
