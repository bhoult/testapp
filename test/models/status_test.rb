require "test_helper"

class StatusTest < ActiveSupport::TestCase
  test "status count" do
    assert_equal 4, Status.count
  end
end
