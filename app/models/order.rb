class Order < ApplicationRecord
    belongs_to :status
    belongs_to :school
    has_and_belongs_to_many :gifts # Allow duplicate gifts 
    has_and_belongs_to_many :recipients, before_add: [
        :validate_recipient_not_already_in_order, 
        :validate_recipient_is_in_orders_school, 
        :validate_max_20_recipients_per_order
    ]

    validates :school, presence: true 
    validates :gifts, presence: true
    validates :recipients, presence:true
    validate :validate_max_school_orders

    # No more than 60 orders per school per day
    def validate_max_school_orders
        if self.school.nil? 
            errors.add(:order, "Order must belong to a school")
        elsif (self.school.total_gifts_today > 60) then
            errors.add(:order, "Maximum of 60 gifts per school for today exceeded.")
        end
    end

    # Don't allow adding more than 20 recipients to an order
    def validate_max_20_recipients_per_order(recipient)
        if self.recipients.count >= 20 
            errors.add(:base, "Order can have a maximum of 20 recipients")
            throw(:abort)
        end 
    end

    # Don't allow the same recipients to be added to an order multiple times
    def validate_recipient_not_already_in_order(recipient)
        if self.recipients.member?(recipient)
            errors.add(:base, "Order Recipients must be unique")
            throw(:abort)
        end 
    end

    # Don't allow a recipient from another school to be added to an order
    def validate_recipient_is_in_orders_school(recipient)
        if self.school != recipient.school
            errors.add(:base, "Order Recipients must be in the same school as the order")
            throw(:abort)
        end
    end

    # The total number of gifts for this order NOTE: may need to optimize
    def total_gifts
        self.recipients.count * self.gifts.count
    end

    def send_shipped_notification
        if self.email_recipients
            # TODO implement sending of emails to all order recipients that their order has shipped
        end
    end

    def self.create_random 
        # Cant save until a school is added.
        Order.new(
            status: Status.all.sample,
            notes: 'Randomly Generated Fake Record'
        )
    end
end
