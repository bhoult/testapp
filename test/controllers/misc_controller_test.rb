require "test_helper"

class MiscControllerTest < ActionDispatch::IntegrationTest
  test "should get html site index" do
    get "/", as: :html
    assert_response :success
    assert_select "h1", "Aptegy Test App"
  end
end
