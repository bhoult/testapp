class RecipientsController < ApplicationController
    before_action :authorized

    # GET /schools/:id/recipients
    # curl -X GET -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1/recipients | json_pp
    def index
        slog("List Recipients")
        render json: Recipient.where(school_id: params[:id]).to_json(:include => [:address] )
    end

    # GET /schools/:id/recipients/:rid
    # curl -X GET -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1/recipients/1 | json_pp
    def read
        slog("Get a Recipient")
        render json: Recipient.where(school_id: params[:id]).where(id: params[:rid]).first.to_json(:include => [:address] )
    end
  
    # POST /schools/:id/recipients
    # curl -X POST -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1/recipients -H 'Content-Type: application/json' -d '{"name":"recipient_test","street1":"somestreet","city":"somecity","state":"arkansas","zip":"12345"}' | json_pp
    def create
        recipient = Recipient.new(
            name: params[:name],
            email: params[:email],
            notes: params[:notes],
            school: School.find_by(id: params[:id])
        )
        address = Address.new(
            street1: params[:street1],
            street2: params[:street2],
            city: params[:city],
            state: params[:state],
            zip: params[:zip],
            country: params[:country],
            notes: params[:address_notes]
        )
        recipient.address = address
        
        # Validate both so it will report both errors
        recipient_valid = recipient.valid?
        address_valid = address.valid?

        if recipient_valid && address_valid
            recipient.save
            slog("Create a recipient")
            render json: recipient.to_json(:include => [:address] )
        else 
            elog("Create a recipient")
            render json: {recipient: recipient.errors, address: address.errors}, status: 500
        end
    end

    # PATCH /schools/:id/recipients/:rid
    # curl -X PATCH -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1/recipients/1 -H 'Content-Type: application/json' -d '{"name":"recipient_test","street1":"somestreet","city":"somecity","state":"arkansas","zip":"12345"}' | json_pp
    def update
        recipient = Recipient.find_by(id: params[:rid])
        unless recipient.nil?
            recipient.update(
                name: params[:name],
                notes: params[:notes],
                school: School.find_by(id: params[:id])
            )
            address = recipient.address
            address.update(
                street1: params[:street1],
                street2: params[:street2],
                city: params[:city],
                state: params[:state],
                zip: params[:zip],
                country: params[:country],
                notes: params[:address_notes]
            )
        
            # Validate both so it will report both errors
            recipient_valid = recipient.valid?
            address_valid = address.valid?

            if recipient_valid && address_valid
                slog("Update a recipient")
                render json: recipient.to_json(:include => [:address] )
            else 
                elog("Update a recipient")
                render json: {recipient: recipient.errors, address: address.errors}.to_json, status: 500
            end
        else
            elog("Update a recipient")
            render json: {error: "Could not find recipient with id #{params[:rid]} to update"}, status:500
        end
    end

    # DELETE /schools/:id/recipients/:rid
    # curl -X DELETE -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1/recipients/1 | json_pp
    def delete
        recipient = Recipient.where(id: params[:rid]).where(school_id: params[:id])
        unless recipient.empty?
            recipient.first.destroy!
            slog("Delete a recipient")
            render json: recipient.first.to_json(:include => [:address] )
        else
            elog("Delete a recipient")
            render json: {error: "Could not find recipient with id #{params[:rid]} to delete"}, status: 500
        end
    end
          
end