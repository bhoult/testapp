class SchoolsController < ApplicationController
    before_action :authorized

    # GET /schools
    # curl -X GET -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools | json_pp
    def index
        slog("List all schools")
        render json: School.all.to_json(:include => [:address] )
    end

    # GET /schools/:id
    # curl -X GET -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1 | json_pp
    def read
        slog("Show a school")
        render json: School.find(params[:id]).to_json(:include => [:address] )
    end
  
    # POST /schools
    # curl -X POST -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools -H 'Content-Type: application/json' -d '{"name":"school_test","street1":"somestreet","city":"somecity","state":"arkansas","zip":"12345"}' | json_pp
    def create
        school = School.new(
            name: params[:name],
            notes: params[:notes]
        )
        address = Address.new(
            street1: params[:street1],
            street2: params[:street2],
            city: params[:city],
            state: params[:state],
            zip: params[:zip],
            country: params[:country],
            notes: params[:address_notes]
        )
        school.address = address

        # Validate both so it will report both errors
        school_valid = school.valid?
        address_valid = address.valid?

        if school_valid && address_valid
            school.save
            slog("Add a school")
            render json: school.to_json(:include => [:address] )
        else 
            elog("Add a school")
            render json: {school: school.errors, address: address.errors}, status: 500
        end
    end

    # PATCH /schools/:id
    # curl -X PATCH -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1 -H 'Content-Type: application/json' -d '{"name":"school_test","street1":"somestreet","city":"somecity","state":"arkansas","zip":"12345"}' | json_pp
    def update
        school = School.find(params[:id])
        school.update(
            name: params[:name],
            notes: params[:notes]
        )
        address = school.address
        address.update(
            street1: params[:street1],
            street2: params[:street2],
            city: params[:city],
            state: params[:state],
            zip: params[:zip],
            country: params[:country],
            notes: params[:address_notes]
        )
    
        # Validate both so it will report both errors
        school_valid = school.valid?
        address_valid = address.valid?

        if school_valid && address_valid
            slog("Update a school")
            render json: school.to_json(:include => [:address] )
        else 
            elog("Update a school")
            render json: {school: school.errors, address: address.errors}.to_json, status: 500
        end
    end

    # DELETE /schools/:id
    # curl -X DELETE -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/schools/1 | json_pp
    def delete
        school = School.where(id: params[:id])
        unless school.empty?
            school.first.destroy!
            slog("Delete a school")
            render json: school.first.to_json(:include => [:address] )
        else
            elog("Delete a school")
            render json: {school: "Could not find school with id #{params[:id]} to delete"}, status: 500
        end
    end
          
end