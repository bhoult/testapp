require "test_helper"

class UsersControllerTest < ActionDispatch::IntegrationTest

  def setup
    # Everything is gonna need a token!
    @user = User.create(username:'admin', firstname:'Admin', lastname:'User', email:'adminuser@email.com', password:'test', notes:"Default Admin User", admin:true)
    post "/users/login", params: {username:"admin", password:"test"}, as: :multipart_form
    @token = JSON.parse(body)["token"] 
  end

  test "should get test without token" do
    get "/test", as: :json
    assert_response :success
    assert_match  /test/, body
  end

  test "should be able to login and get a token" do
    assert_not_nil @token, "We should have a authentication token"
    assert_response :success
    assert_match  /admin/, body
  end

  test "should be able to get the index of users" do
    get "/users", headers: {Authorization: "Bearer #{@token}"}
    assert_response :success
    assert_match  /admin/, body
  end

  test "should be able to get a users info" do
    get "/users/#{@user.id}", headers: {Authorization: "Bearer #{@token}"}
    assert_response :success
    assert_match  /admin/, body
  end

  test "should be able to create a new user as admin" do
    post "/users", headers: {Authorization: "Bearer #{@token}"}, params: {username:"test2", password:"test2", admin:"false"}, as: :multipart_form
    assert_response :success
    assert_match  /test2/, body
    assert User.count == 2
  end

  test "should not be able to create a new user as non-admin" do
    post "/users", headers: {Authorization: "Bearer #{@token}"}, params: {username:"test2", password:"test2", admin:"false"}, as: :multipart_form
    post "/users/login", params: {username:"test2", password:"test2"}, as: :multipart_form
    token = JSON.parse(body)["token"] 
    post "/users", headers: {Authorization: "Bearer #{token}"}, params: {username:"test3", password:"test3", admin:"false"}, as: :multipart_form
    assert_response :error
    assert_match  /Only Admin/, body
    assert User.count == 2
  end

end
