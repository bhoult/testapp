class School < ApplicationRecord
    has_one :address, :as => :addressable, dependent: :destroy
    has_many :orders
    has_many :recipients

    validates :name, presence: true
    validates :name, uniqueness: true
    validates :address, presence: true

    def self.create_random
        School.create(
            name: Faker::Educator.primary_school,
            address: Address.create_random,
            notes: 'Randomly Generated Fake Record'
        )
    end

    def total_gifts_today
        self.orders.where(created_at: Time.current.all_day).collect{|o| o.total_gifts}.sum
    end
end
