class Address < ApplicationRecord
    belongs_to :addressable, :polymorphic => true

    validates :street1, presence: true
    validates :city, presence: true
    validates :state, presence: true
    validates :zip, presence: true

    def self.create_random
        Address.create(
            street1: Faker::Address.street_address,
            street2: Faker::Address.secondary_address,
            city: Faker::Address.city,
            state: Faker::Address.state,
            zip: Faker::Address.zip,
            country: "USA", 
            notes: 'Randomly Generated Fake Record'
        )
    end
end
