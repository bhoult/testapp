require "test_helper"

class AddressTest < ActiveSupport::TestCase
  
  def setup
    @address = Address.create_random # polymorphic must be associated with something.
    @school = School.create_random
    @school.address = @address
  end

  test "setup_address_is_valid" do
    assert @address.valid? == true, "Setup address is not valid"
  end

  test "address_must_have_a_street1" do
    @address.street1 = ""
    refute @address.valid?, "Address valid without a street1"
  end

end
