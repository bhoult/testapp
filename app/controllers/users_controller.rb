# https://betterprogramming.pub/build-a-rails-api-with-jwt-61fb8a52d833

class UsersController < ApplicationController
  before_action :authorized, except: [:login, :test]

  # REGISTER
  # POST /users
  # curl -X POST -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/users -H 'Content-Type: application/json' -d '{"username":"test2","password":"test2"}' | json_pp
  def create
    if @user.admin? 
      @user = User.create(
        username: params[:username],
        password: params[:password],
        firstname: params[:firstname],
        lastname: params[:lastname],
        email: params[:email],
        admin: params[:admin],
        notes: params[:notes]
      )
      if @user.valid?
        token = encode_token({user_id: @user.id})
        render json: {user: @user, token: token}
      else
        render json: {error: "Invalid username or password"}, status: 500
      end
    else
      render json: {error: "Only Admin can create new users"}, status: 500
    end
  end

  # LOGGING IN
  # POST /users/login
  # curl -X POST localhost:3000/users/login -H 'Content-Type: application/json' -d '{"username":"admin","password":"test"}'| json_pp
  def login
    @user = User.find_by(username: params[:username])

    if @user && @user.authenticate(params[:password])
      token = encode_token({user_id: @user.id})
      slog("user login")
      render json: {user: @user, token: token}
    else
      elog("user login")
      render json: {error: "Invalid username or password"}, status: 500
    end
  end

  def auto_login
    render json: @user
  end

  ##########################################

  
  # GET /users
  # curl -X GET -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/users | json_pp
  def index
    slog("users index")
    render json: User.all
  end

  # GET /users/:id
  # curl -X GET -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/users/1 | json_pp
  def read
    slog("user info")
    render json: User.find(params[:id])
  end

  # Just for testing random stuff
  # curl -X GET -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/users/test | json_pp
  def test
    slog("user test")
    render json: {test: 'test'}
  end

  private

  def user_params
    params.permit(:username, :password, :firstname, :lastname, :email, :notes, :admin)
  end

end