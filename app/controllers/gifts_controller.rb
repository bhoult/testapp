# Made this a model/controller assuming that the gifts might change in the future

class GiftsController < ApplicationController
    before_action :authorized

    # GET /gifts
    # curl -X GET -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ._fY8DlihYIcYLaTI2BRNSpcErBudi8r-MGENoLGvJy8' localhost:3000/gifts | json_pp
    def index
        slog("Gifts Index")
        render json: Gift.all
    end
end