require "test_helper"

class GiftsControllerTest < ActionDispatch::IntegrationTest

  def setup
    # Everything is gonna need a token!
    @user = User.create(username:'admin', firstname:'Admin', lastname:'User', email:'adminuser@email.com', password:'test', notes:"Default Admin User", admin:true)
    post "/users/login", params: {username:"admin", password:"test"}, as: :multipart_form
    @token = JSON.parse(body)["token"] 
  end

  test "should not be able to hit anything without token" do
    get "/gifts"
    assert_response :unauthorized
    assert_match  /log in/, body
  end

  test "should get index of gifts with token" do
    get "/gifts", headers: {Authorization: "Bearer #{@token}"}
    assert_response :success
    assert_match  /HOODIE/, body
  end

end

