class CreateOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :orders do |t|
      t.text :notes
      t.bigint :status_id
      t.bigint :school_id
      t.boolean :email_recipients

      t.timestamps
    end
  end
end
