# https://betterprogramming.pub/build-a-rails-api-with-jwt-61fb8a52d833

class ApplicationController < ActionController::Base
  skip_before_action :verify_authenticity_token # Don't use CRSF token since this is promarily an api
  
  before_action :authorized

  def encode_token(payload)
    JWT.encode(payload, Rails.configuration.secret)
  end

  def auth_header
    # { Authorization: 'Bearer <token>' }
    h = request.headers['Authorization']
    
    # if there is no auth header then try gettin it from the 'bearer' url parameter
    if h.nil? && params[:bearer]
      h = "Bearer #{params[:bearer]}"
    end
    return h
  end

  def decoded_token
    if auth_header
      token = auth_header.split(' ')[1]
      # header: { 'Authorization': 'Bearer <token>' }
      begin
        JWT.decode(token, Rails.configuration.secret, true, algorithm: 'HS256')
      rescue JWT::DecodeError
        nil
      end
    end
  end

  def logged_in_user
    if decoded_token
      user_id = decoded_token[0]['user_id']
      @user = User.find_by(id: user_id)
    end
  end

  def logged_in?
    !!logged_in_user
  end

  def authorized
    render json: { message: 'Please log in' }, status: :unauthorized unless logged_in?
  end

  def mytime
    Time.now.strftime("%-d/%-m/%y: %H:%M %Z")
  end

  # Custom Logging of Success ?? meaningful ??
  def slog(s)
    logger.info("\n--- SUCCESS --- #{mytime} --- User: #{begin @user.username rescue "" end} --- Called: #{s} --- With: #{params}\n")
    return true
  end

  # Custom Logging of Error
  def elog(s)
    logger.info("\n--- *ERROR* --- #{mytime} --- User: #{begin @user.username rescue "" end} --- Called: #{s} --- With: #{params}\n")
    return true
  end
end
