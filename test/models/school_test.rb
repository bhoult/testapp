require "test_helper"

class SchoolTest < ActiveSupport::TestCase

  def setup
    @school = School.create(name:"testschool")
    @address = Address.create_random
    @school.address = @address

    @recipient = Recipient.create_random
    @recipient_address = Address.create_random
    @recipient.address = @recipient_address
    @recipient.school = @school
    @recipient.save

    @order = Order.create_random
    @order.school = @school
    @order.recipients << @recipient
    @order.gifts << Gift.first
    @order.save

    @school.save!
  end

  test "setup school is valid" do
    assert @school.valid? == true, "School in setup is not valid"
  end

  test "school must have a name" do
    @school.name = nil
    refute @school.valid?, "School was saved without a name"
    assert_not_nil @school.errors[:name], 'no validation error for school name'
  end

  test "school name must be unique" do
    school = School.create(name:"testschool")
    refute school.valid?, "School was saved with a duplicate name"
    assert_not_nil school.errors[:name], 'no validation error for duplicate school name'
  end

  test "school must have an address" do
    @school.address = nil
    refute @school.valid?, "School was saved without an address"
    assert_not_nil @school.errors, 'no validation error for school missing address'
  end

  test "school has orders" do
    assert @school.orders, "School is missing orders"
  end

  test "school has recipients" do
    assert @school.recipients, "School is missing recipients"
  end

  test "school can have no orders" do
    @school.orders.delete
    assert @school.valid?, "School invalid without orders"
  end

  test "school can have no recipients" do
    @school.recipients.delete
    assert @school.valid?, "School invalid without recipients"
  end

end