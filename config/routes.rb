Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  root "misc#index"

  #resources :users
  get "/users" => "users#index"
  get "/users/:id" => "users#read"
  get "/test" => "users#test"
  post "/users" => "users#create"
  post "/users/login" => "users#login"

  #resources :schools
  get "/schools" => "schools#index"         # index
  get "/schools/:id" => "schools#read"      # read
  post "/schools" => "schools#create"       # create
  patch "/schools/:id" => "schools#update"  # update
  delete "/schools/:id" => "schools#delete" # delete

  #resources :recipients
  # All recipient operations are scoped to a school
  get "/schools/:id/recipients" => "recipients#index"          # index
  get "/schools/:id/recipients/:rid" => "recipients#read"      # read
  post "/schools/:id/recipients" => "recipients#create"        # create
  patch "/schools/:id/recipients/:rid" => "recipients#update"  # update
  delete "/schools/:id/recipients/:rid" => "recipients#delete" # delete

  #resources :orders
  # All order operations are scoped to a school
  get "/schools/:id/orders" => "orders#index"                                           # index
  get "/schools/:id/orders/:oid" => "orders#read"                                       # read
  post "/schools/:id/orders" => "orders#create"                                         # create
  patch "/schools/:id/orders/:oid" => "orders#update"                                   # update
  delete "/schools/:id/orders/:oid" => "orders#delete"                                  # delete
  patch "/schools/:id/orders/:oid/cancel" => "orders#cancel"                            # cancel order
  patch "/schools/:id/orders/:oid/ship" => "orders#ship"                                # ship order
  # Thought it would be simpler to allow adding and removing individual gifts and recipients from orders
  post "/schools/:id/orders/:oid/add_recipient/:rid" => "orders#add_recipient"          # add recipient
  delete "/schools/:id/orders/:oid/remove_recipient/:rid" => "orders#remove_recipient"  # remove recipient
  post "/schools/:id/orders/:oid/add_gift/:gid" => "orders#add_gift"                    # add gift
  delete "/schools/:id/orders/:oid/remove_gift/:gid" => "orders#remove_gift"            # remove gift

  #resources :gifts
  get "/gifts" => "gifts#index"

  #resources :statuses
  get "/statuses" => "statuses#index"
end