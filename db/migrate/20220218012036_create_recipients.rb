class CreateRecipients < ActiveRecord::Migration[7.0]
  def change
    create_table :recipients do |t|
      t.string :name
      t.string :email
      t.text :notes
      t.bigint :school_id

      t.timestamps
    end
  end

  # HABTM table 
  create_table :recipients_schools, id: false do |t|
    t.belongs_to :recipient
    t.belongs_to :school
  end

  # HABTM table 
  create_table :orders_recipients, id: false do |t|
    t.belongs_to :order
    t.belongs_to :recipient
  end
end
