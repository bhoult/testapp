#resources :recipients
# All recipient operations are scoped to a school
#get "/schools/:id/recipients" => "recipients#index"          # index
#get "/schools/:id/recipients/:rid" => "recipients#read"      # read
#post "/schools/:id/recipients" => "recipients#create"        # create
#patch "/schools/:id/recipients/:rid" => "recipients#update"  # update
#delete "/schools/:id/recipients/:rid" => "recipients#delete" # delete

require "test_helper"

class RecipientsControllerTest < ActionDispatch::IntegrationTest

  def setup
    # Everything is gonna need a token!
    @user = User.create(username:'admin', firstname:'Admin', lastname:'User', email:'adminuser@email.com', password:'test', notes:"Default Admin User", admin:true)
    post "/users/login", params: {username:"admin", password:"test"}, as: :multipart_form
    @token = JSON.parse(body)["token"] 

    @recipient = Recipient.create_random
    @address = Address.create_random
    @school = School.create_random
    
    @school.address = @address
    @school.save!
    @recipient.address = @address
    @recipient.school = @school
    @recipient.save!
  end

  test "should not be able to hit anything without token" do
    get "/schools/#{@school.id}/recipients"
    assert_response :unauthorized
    assert_match  /log in/, body

    get "/schools/#{@school.id}/recipients/#{@recipient.id}"
    assert_response :unauthorized
    assert_match  /log in/, body

    post "/schools/#{@school.id}/recipients"
    assert_response :unauthorized
    assert_match  /log in/, body

    patch "/schools/#{@school.id}/recipients/#{@recipient.id}"
    assert_response :unauthorized
    assert_match  /log in/, body

    delete "/schools/#{@school.id}/recipients/#{@recipient.id}"
    assert_response :unauthorized
    assert_match  /log in/, body
  end

  test "get recipients index" do
    get "/schools/#{@school.id}/recipients", headers: {Authorization: "Bearer #{@token}"}
    assert_response :success
    assert_match  /Randomly/, body
  end

  test "get a recipient info" do
    get "/schools/#{@school.id}/recipients/#{@recipient.id}", headers: {Authorization: "Bearer #{@token}"}
    assert_response :success
    assert_match  /Randomly/, body
  end

  test "create a recipient" do
    post "/schools/#{@school.id}/recipients", headers: {Authorization: "Bearer #{@token}"}, 
        params: {name:"recipient_test",street1:"somestreet",city:"somecity",state:"arkansas",zip:"12345"}, as: :multipart_form
    assert_response :success
    assert Recipient.count == 2
    assert_match  /recipient_test/, body
  end

  test "update a recipient" do
    patch "/schools/#{@school.id}/recipients/#{@recipient.id}", headers: {Authorization: "Bearer #{@token}"}, 
        params: {name:"recipient_test",street1:"somestreet",city:"somecity",state:"arkansas",zip:"12345"}, as: :multipart_form
    assert_response :success
    assert School.count == 1
    assert_match  /recipient_test/, body
  end

  test "delete a recipient" do
    delete "/schools/#{@school.id}/recipients/#{@recipient.id}", headers: {Authorization: "Bearer #{@token}"}
    assert_response :success
    assert Recipient.count == 0
    assert_match  /#{@recipient.name}/, body
  end

end

